<!-- TITLE: It Services -->
<!-- SUBTITLE: A quick summary of It Services -->

# Mail client settings 
Введите следующую информацию:
Email address (Адрес электронной почты): введите свой адрес электронной почты в домене @edu.innopolis.ru example@edu.innopolis.ru.
Password (Пароль): введите пароль от своего почтового ящика.

Нажмите Next (Далее).
Выберите учетную запись Exchange.
Укажите следующие настройки сервера Exchange:
Domain\Username (Домен\Имя пользователя): введите example@innopolis.ru
Password (Пароль): введите свой пароль от почтового ящика

Server (Сервер): введите имя сервера ActiveSync mail.university.innopolis.ru.
Выберите опцию Use secure connection (SSL) (Использовать безопасное соединение (SSL)).
Оставьте невыбранной опцию Accept all SSL certificates (Принимать все сертификаты SSL).

Нажмите Next (Далее).
Укажите следующие опции (или оставьте значения по умолчанию):
Email checking frequency (Частота проверки почты) (Automatic (автоматически)/Never (никогда)/Every 5 minutes (каждые 5 минут)/Every 10 minutes (каждые 10 минут)/Every 15 minutes (каждые 15 минут)/Every 30 minutes (каждые 30 минут)),
Amount to synchronize (Объем синхронизации) (One day (один день)/Three days (три дня)/One week (одна неделя)/Two weeks (две недели)/One month (один месяц)),
Sent email from this account by default (Отправлять почту с этой учетной записи по умолчанию) (Yes (да)/No (нет)),
Notify me when email arrives (Уведомлять о поступлении почты) (Yes/No),
Sync contacts from this account (Синхронизировать контакты этой учетной записи) (Yes/No)

Нажмите Next (Далее).
Укажите имя учетной записи (необязательно) и свое имя.
Нажмите Done (Готово), чтобы закончить настройку.